<?php

if ($argc < 2) {
    echo 'No search term was specified' . PHP_EOL;
    return;
}

$basePath = __DIR__ . '/../';
require_once $basePath . 'src/functions.php';

$companies = searchCompanies(getCompanyObjects(), $argv[1]);

if (!$companies) {
    echo 'No companies found for search term!' . PHP_EOL;
    return;
}

// path to file (relative from this PHP file)
$filename = $basePath . 'storage/' . $argv[1] . '-' . (new DateTime())->getTimestamp() . '.csv';

// open the file in read mode
// @see http://php.net/fopen for other modes (r, r+, w, w+, a, a+, ...)
$file = new SplFileObject($filename, 'w');

// read as much bytes as the file size
foreach ($companies as $company) {
    $contents = $file->fwrite($company->__toString() . PHP_EOL);
}

// close the file handle
$file = null;