<?php

if ($argc < 2) {
    echo 'No search term was specified' . PHP_EOL;
    return;
}

$basePath = __DIR__ . '/../';
require_once $basePath . 'src/functions.php';

$companies = searchCompanies(getCompanyObjects(), $argv[1]);

if (!$companies) {
    echo 'No companies found for search term!' . PHP_EOL;
    return;
}

foreach ($companies as $company) {
    echo $company->__toString() . PHP_EOL;
}