<?php

require_once __DIR__ . '/../vendor/autoload.php';
$basePath = __DIR__ . '/../';
require_once $basePath . 'src/functions.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

// General variables
$basePath = __DIR__ . '/../';

$errorName = '';
$errorAddress = '';
$errorCity = '';
$errorVAT = '';
$errorActivity = '';
$errors = false;

if (isset($_POST['btnSubmit'])) {
    if (!$_POST['name']) {
        $errorName = 'Name is required!';
        $errors = true;
    }

    if (!$_POST['address']) {
        $errorAddress = 'Address is required!';
        $errors = true;
    }

    if (!$_POST['city']) {
        $errorCity = 'City is required!';
        $errors = true;
    }

    if (!$_POST['zip']) {
        $errorCity = 'Zip is required!';
        $errors = true;
    }

    if (!$_POST['city'] && !$_POST['zip']) {
        $errorCity = 'City and zip are required!';
        $errors = true;
    }

    if (!$_POST['vat']) {
        $errorVAT = 'VAT number is required!';
        $errors = true;
    }

    if (!$_POST['activity']) {
        $errorActivity = 'Activity is required!';
        $errors = true;
    }

    if (!$errors) {
        $connection = getDBConnection();
        $stmt = $connection->prepare('INSERT INTO `companies`(`name`, `address`, `zip`, `city`, `activity`, `vat`) VALUES (?, ?, ?, ?, ?, ?)');
        $stmt->execute([
            $_POST['name'],
            $_POST['address'],
            $_POST['zip'],
            $_POST['city'],
            $_POST['activity'],
            $_POST['vat']
        ]);

        /*
        $companiesArray = require __DIR__ . '/../resources/data/companies.php';
        $companiesArray[] = [
            'name' => $_POST['name'],
            'address' => $_POST['address'],
            'zip' => $_POST['zip'],
            'city' => $_POST['city'],
            'activity' => $_POST['activity'],
            'vat' => $_POST['vat']
        ];*/
/*
        //echo(print_r($companiesArray, true)); die;
        // path to file (relative from this PHP file)
        $filename = $basePath . '/resources/data/companies.php';

        // open the file in read mode
        // @see http://php.net/fopen for other modes (r, r+, w, w+, a, a+, ...)
        $file = new SplFileObject($filename, 'w');

        // read as much bytes as the file size
        $file->fwrite('<?php return' . PHP_EOL);
        $file->fwrite(var_export($companiesArray, true));
        $file->fwrite(';');

        // close the file handle
        $file = null;*/
        header('Location: companies.php');
    }
}

// View
echo $twig->render('pages/add-company.twig', [
    'name' => $_POST['name'] ?? '',
    'address' => $_POST['address'] ?? '',
    'zip' => $_POST['zip'] ?? '',
    'city' => $_POST['city'] ?? '',
    'activity' => $_POST['activity'] ?? '',
    'vat' => $_POST['vat'] ?? '',
    'errorName' => $errorName,
    'errorAddress' => $errorAddress,
    'errorCity' => $errorCity,
    'errorVAT' => $errorVAT,
    'errorActivity' => $errorActivity
]);