<?php

require_once __DIR__ . '/../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

// General variables
$basePath = __DIR__ . '/../';

require_once $basePath . 'src/functions.php';

// Data
$companies = getCompanyObjects();
usort($companies, static function ($a, $b) {
    return $a->getName() <=> $b->getName();
});

$errorTitle = '';
$errorCompany = '';
$errorDate = '';
$errorShortDescription = '';
$errorPriority = '';
$errorResponsible = '';
$errors = false;

if (isset($_POST['btnSubmit'])) {
    if (!$_POST['title']) {
        $errorTitle = 'This field is required!';
        $errors = true;
    }

    if (!$_POST['company']) {
        $errorCompany = 'This field is required!';
        $errors = true;
    }

    if (!$_POST['date']) {
        $errorDate = 'This field is required!';
        $errors = true;
    }

    if (!$_POST['short-description']) {
        $errorShortDescription = 'This field is required!';
        $errors = true;
    }

    if (!$_POST['priority']) {
        $errorPriority = 'This field is required!';
        $errors = true;
    }

    if (!$_POST['responsible-customer']) {
        $errorResponsible = 'This field is required!';
        $errors = true;
    }

    $documentName = '';
    if (isset($_FILES['document']) && ($_FILES['document']['error'] === UPLOAD_ERR_OK)) {
        if (in_array(
            strtolower((new SplFileInfo($_FILES['document']['name']))->getExtension()),
            ['jpeg', 'jpg', 'png', 'doc', 'docx', 'xls', 'xlsx', 'pdf']
        )) {
            $moved = @move_uploaded_file(
                $_FILES['document']['tmp_name'],
                __DIR__ . '/../resources/data/tickets/documents/BE0400106291/' . $_FILES['document']['name']
            );
            $documentName = 'resources/data/tickets/documents/BE0400106291/' . $_FILES['document']['name'];
            if (!$moved) {
                $errors = true;
                echo('<p>Error while saving file in the uploads folder</p>');
            }
        } else {
            $errors = true;
            echo('<p>Invalid extension. Only .jpeg, .jpg, .png or .gif allowed</p>');
        }
    }

    if (!$errors) {
        //echo(print_r($companiesArray, true)); die;
        // path to file (relative from this PHP file)
        $filename = $basePath . '/resources/data/tickets/BE0400106291.csv';

        // open the file in read mode
        // @see http://php.net/fopen for other modes (r, r+, w, w+, a, a+, ...)
        $file = new SplFileObject($filename, 'a+');

        var_dump($_POST['title'] . ';' .
            $_POST['company'] . ';' .
            $_POST['date'] . ';' .
            $_POST['short-description'] . ';' .
            $_POST['long-description'] . ';' .
            $_POST['preferred-situation'] . ';' .
            $_POST['priority'] . ';' .
            $_POST['responsible-customer'] . ';' .
            $documentName . PHP_EOL); die;

        // read as much bytes as the file size
        $file->fwrite(
            $_POST['title'] . ';' .
            $_POST['company'] . ';' .
            $_POST['date'] . ';' .
            $_POST['short-description'] . ';' .
            $_POST['long-description'] . ';' .
            $_POST['preferred-situation'] . ';' .
            $_POST['priority'] . ';' .
            $_POST['responsible-customer'] . ';' .
            $documentName . PHP_EOL
        );
        //document

        // close the file handle
        $file = null;
        header('Location: companies.php');
    }
}

// View
echo $twig->render('pages/add-ticket.twig', [
    'companies' => $companies,
    'errorTitle' => $errorTitle,
    'errorCompany' => $errorCompany,
    'errorDate' => $errorDate,
    'errorShortDescription' => $errorShortDescription,
    'errorPriority' => $errorPriority,
    'errorResponsible' => $errorResponsible,
    'persistedTitle' => $_POST['title'] ?? '',
    'persistedCompany' => $_POST['company'] ?? '',
    'persistedDate' => $_POST['date'] ?? date('Y-m-d'),
    'persistedShortDescription' => $_POST['short-description'] ?? '',
    'persistedLongDescription' => $_POST['long-description'] ?? '',
    'persistedPreferredSituation' => $_POST['preferred-situation'] ?? '',
    'persistedPriority' => $_POST['priority'] ?? '',
    'persistedResponsibleCustomer' => $_POST['responsible-customer'] ?? ''
]);