<?php
require_once __DIR__ . '/../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

// General variables
$basePath = __DIR__ . '/../';

require_once $basePath . 'src/Models/Company.php';
require_once $basePath . 'src/functions.php';

// Data
$companies = getDBCompanyObjects();

/*$companies = getCompanyObjects();*/
$cities = getCityObjects();
$errorSearch = null;
$errorCity = null;
$errorProvince = null;

$provincesAsInput  = [
    9 => 'Oost-Vlaanderen',
    8 => 'West-Vlaanderen',
    1 => 'Vlaams-Brabant',
    2 => 'Antwerpen',
    3 => 'Limburg'
];
/*
usort($companies, static function ($a, $b) {
    return $a->getName() <=> $b->getName();
});*/

$contacts = getContactObjects();

usort($contacts, static function ($a, $b) {
    return $a->getName() <=> $b->getName();
});

if (isset($_GET['term']) && $_GET['term'] !== '') {
    if (!preg_match('/^[a-zA-Z0-9_]+$/', $_GET['term'])) {
        $errorSearch = 'Dit is geen geldige zoekopdracht';
    } else {
        $companies = searchDBCompanies($_GET['term']);
    }
}

if (isset($_GET['city']) && $_GET['city'] !== '') {
    $companies = searchDBCompaniesByCity($_GET['city'], $_GET['term']);
}

if (isset($_GET['province']) && $_GET['province'] !== '') {
    $companies = searchCompaniesByProvinces($companies, $_GET['province']);
}

if (array_key_exists('term', $_GET) && $_GET['term'] !== '') {
    if (preg_match('/^[a-zA-Z0-9_]+$/', $_GET['term'])) {
        $contacts = searchContacts($contacts, $_GET['term']);
    }
}

// View
echo $twig->render('pages/companies.twig', [
    'cities' => $cities,
    'errorCity' => $errorCity,
    'errorProvince' => $errorProvince,
    'errorSearch' => $errorSearch,
    'companies' => $companies,
    'contacts' => $contacts,
    'selectedCity' => $_GET['city'] ?? '',
    'selectedProvinces' => $_GET['province'] ?? '',
    'provinces' => $provincesAsInput,
    'searchTerm' => $_GET['term'] ?? ''
]);
