<?php
require_once __DIR__ . '/../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

// General variables
$basePath = __DIR__ . '/../';


require_once $basePath . 'src/Models/Company.php';

// Validation


require_once $basePath . 'src/functions.php';
require_once $basePath . 'config/database.php';

// Data
$connection = getDBConnection();
$stmt = $connection->prepare('SELECT * FROM companies WHERE `id` LIKE ?');
$stmt->execute([$_GET['id']]);
$companiesArray = $stmt->fetchAllAssociative();



// Data
$company = null;
foreach ($companiesArray as $companyFromArray) {

        $company = new Company(
            $companyFromArray['name'],
            $companyFromArray['address'],
            $companyFromArray['zip'],
            $companyFromArray['city'],
            $companyFromArray['activity'],
            $companyFromArray['vat'],
            $companyFromArray['id']
        );

}
/*
$companies = getCompanyObjects();
$company = [];
foreach ($companies as $companyObj) {
    if ($companyObj->getName() === $_GET['name']) {
        $company = $companyObj;
        break;
    }
}

// Data
$companiesArray = require $basePath . 'resources/data/companies.php';
$company = null;
foreach ($companiesArray as $companyFromArray) {
    if ($companyFromArray['name'] === $_GET['name']) {
        $company = new Company(
            $companyFromArray['name'],
            $companyFromArray['address'],
            $companyFromArray['zip'],
            $companyFromArray['city'],
            $companyFromArray['activity'],
            $companyFromArray['vat']
        );
    }
}
*/
if (!$company) {
    header('Location: /companies.php');
}

// View
echo $twig->render('pages/company.twig', ['company' => $company]);
