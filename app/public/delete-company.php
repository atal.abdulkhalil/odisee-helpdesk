<?php

require_once __DIR__ . '/../vendor/autoload.php';
$basePath = __DIR__ . '/../';
require_once $basePath . 'src/functions.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

// General variables
$basePath = __DIR__ . '/../';

$id = (isset($_GET['id'])? $_GET['id'] : '');
$connection = getDBConnection();
$stmt = $connection->prepare('SELECT * FROM companies WHERE `id` LIKE ?');
$stmt->execute([$id]);
$companiesArray = $stmt->fetchAllAssociative();


$company = null;
foreach ($companiesArray as $companyFromArray) {
    $company = new Company(
        $companyFromArray['name'],
        $companyFromArray['address'],
        $companyFromArray['zip'],
        $companyFromArray['city'],
        $companyFromArray['activity'],
        $companyFromArray['vat'],
        $companyFromArray['id']
    );
}

if (! $company) {
    header('Location: /companies.php');
    exit;
}


if (isset($_POST['btnSubmit'])) {
        $stmt = $connection->prepare('DELETE FROM `companies` WHERE `companies`.`id` = ?');
        $stmt->execute([$_GET['id']]);
        header('Location: companies.php');
}

// View
echo $twig->render('pages/delete-company.twig');