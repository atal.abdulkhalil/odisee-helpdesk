<?php

require_once __DIR__ . '/../vendor/autoload.php';
$basePath = __DIR__ . '/../';
require_once $basePath . 'src/functions.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

// General variables
$basePath = __DIR__ . '/../';

$id = (isset($_GET['id'])? $_GET['id'] : '');
$connection = getDBConnection();
$stmt = $connection->prepare('SELECT * FROM companies WHERE `id` LIKE ?');
$stmt->execute([$id]);
$companiesArray = $stmt->fetchAllAssociative();


$company = null;
foreach ($companiesArray as $companyFromArray) {
    $company = new Company(
        $companyFromArray['name'],
        $companyFromArray['address'],
        $companyFromArray['zip'],
        $companyFromArray['city'],
        $companyFromArray['activity'],
        $companyFromArray['vat'],
        $companyFromArray['id']
    );
}

if (! $company) {
    header('Location: /companies.php');
    exit;
}

$errorName = '';
$errorAddress = '';
$errorCity = '';
$errorVAT = '';
$errorActivity = '';
$errors = false;

if (isset($_POST['btnSubmit'])) {
    if (!$_POST['name']) {
        $errorName = 'Name is required!';
        $errors = true;
    }

    if (!$_POST['address']) {
        $errorAddress = 'Address is required!';
        $errors = true;
    }

    if (!$_POST['city']) {
        $errorCity = 'City is required!';
        $errors = true;
    }

    if (!$_POST['zip']) {
        $errorCity = 'Zip is required!';
        $errors = true;
    }

    if (!$_POST['city'] && !$_POST['zip']) {
        $errorCity = 'City and zip are required!';
        $errors = true;
    }

    if (!$_POST['vat']) {
        $errorVAT = 'VAT number is required!';
        $errors = true;
    }

    if (!$_POST['activity']) {
        $errorActivity = 'Activity is required!';
        $errors = true;
    }

    if (!$errors) {
        $stmt = $connection->prepare('UPDATE `companies` SET `name` = ?, `address` = ?, `zip` = ?, `city` = ?, `activity` = ?, `vat` = ? WHERE `id` = ?');
        $stmt->execute([
            $_POST['name'],
            $_POST['address'],
            $_POST['zip'],
            $_POST['city'],
            $_POST['activity'],
            $_POST['vat'],
            $_GET['id']
        ]);
        header('Location: companies.php');
    }
}

// View
echo $twig->render('pages/edit-company.twig', [
    'name' => $_POST['name'] ?? '',
    'address' => $_POST['address'] ?? '',
    'zip' => $_POST['zip'] ?? '',
    'city' => $_POST['city'] ?? '',
    'activity' => $_POST['activity'] ?? '',
    'vat' => $_POST['vat'] ?? '',
    'errorName' => $errorName,
    'errorAddress' => $errorAddress,
    'errorCity' => $errorCity,
    'errorVAT' => $errorVAT,
    'errorActivity' => $errorActivity,
    'company' => $company

]);