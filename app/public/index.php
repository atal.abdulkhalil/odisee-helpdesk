<?php
require __DIR__ . '/../vendor/autoload.php';

// Create Router instance
$router = new \Bramus\Router\Router();
// General variables
$basePath = __DIR__ . '/../';

// Define routes
$router->get('/', 'CompanyController@home' );
$router->get('/companies', 'CompanyController@overview');
$router->get('/lookup', function() {
    $id = (isset($_GET['id'])? $_GET['id'] : '');
    header('Location: companies/' . urlencode($id));
    exit();
});

$router->get('/companies/(\w+)', 'CompanyController@detail');



// Run it!
$router->run();