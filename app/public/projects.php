<?php
require_once __DIR__ . '/../vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
$twig = new \Twig\Environment($loader);

// General variables
$basePath = __DIR__ . '/../';

require_once $basePath . 'src/Models/Company.php';
require_once $basePath . 'src/Models/Project.php';
require_once $basePath . 'src/functions.php';

// Data
$companies = getDBCompanyObjects();

/*$companies = getCompanyObjects();*/
$cities = getCityObjects();
$errorSearch = null;
$errorCity = null;
$errorProvince = null;

$provincesAsInput  = [
    9 => 'Oost-Vlaanderen',
    8 => 'West-Vlaanderen',
    1 => 'Vlaams-Brabant',
    2 => 'Antwerpen',
    3 => 'Limburg'
];



// View
echo $twig->render('pages/projects.twig', [

]);
