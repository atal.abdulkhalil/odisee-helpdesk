<?php


class CompanyController
{
    protected \Doctrine\DBAL\Connection $db;
    protected \Twig\Environment $twig;

    public function __construct()
    {
        // initiate DB connection

        // bootstrap Twig
        $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../resources/templates');
        $this->twig = new \Twig\Environment($loader);
    }

    public function home()
    {
        echo $this->twig->render('pages/index.twig');
    }

    public function overview()
    {
        // Data
        $companies = getDBCompanyObjects();

        /*$companies = getCompanyObjects();*/
        $cities = getCityObjects();
        $errorSearch = null;
        $errorCity = null;
        $errorProvince = null;

        $provincesAsInput  = [
            9 => 'Oost-Vlaanderen',
            8 => 'West-Vlaanderen',
            1 => 'Vlaams-Brabant',
            2 => 'Antwerpen',
            3 => 'Limburg'
        ];
        /*
        usort($companies, static function ($a, $b) {
            return $a->getName() <=> $b->getName();
        });*/

        $contacts = getContactObjects();

        usort($contacts, static function ($a, $b) {
            return $a->getName() <=> $b->getName();
        });

        if (isset($_GET['term']) && $_GET['term'] !== '') {
            if (!preg_match('/^[a-zA-Z0-9_]+$/', $_GET['term'])) {
                $errorSearch = 'Dit is geen geldige zoekopdracht';
            } else {
                $companies = searchDBCompanies($_GET['term']);
            }
        }

        if (isset($_GET['city']) && $_GET['city'] !== '') {
            $companies = searchDBCompaniesByCity($_GET['city'], $_GET['term']);
        }

        if (isset($_GET['province']) && $_GET['province'] !== '') {
            $companies = searchCompaniesByProvinces($companies, $_GET['province']);
        }

        if (array_key_exists('term', $_GET) && $_GET['term'] !== '') {
            if (preg_match('/^[a-zA-Z0-9_]+$/', $_GET['term'])) {
                $contacts = searchContacts($contacts, $_GET['term']);
            }
        }

// View
        echo $this->twig->render('pages/companies.twig', [
            'cities' => $cities,
            'errorCity' => $errorCity,
            'errorProvince' => $errorProvince,
            'errorSearch' => $errorSearch,
            'companies' => $companies,
            'contacts' => $contacts,
            'selectedCity' => $_GET['city'] ?? '',
            'selectedProvinces' => $_GET['province'] ?? '',
            'provinces' => $provincesAsInput,
            'searchTerm' => $_GET['term'] ?? ''
        ]);
    }

    public function detail($id)
    {
        // Validation
        if (! $id) {
            header('Location: /companies.php');
            exit;
        }

// Data
        $connection = getDBConnection();
        $stmt = $connection->prepare('SELECT * FROM companies WHERE `id` LIKE ?');
        $stmt->execute([$id]);
        $companiesArray = $stmt->fetchAllAssociative();



// Data
        $company = null;
        foreach ($companiesArray as $companyFromArray) {
                $company = new Company(
                    $companyFromArray['name'],
                    $companyFromArray['address'],
                    $companyFromArray['zip'],
                    $companyFromArray['city'],
                    $companyFromArray['activity'],
                    $companyFromArray['vat'],
                    $companyFromArray['id']
                );
        }

        if (!$company) {
            header('Location: /companies.php');
        }

// View
        echo $this->twig->render('pages/company.twig', ['company' => $company]);
    }
}