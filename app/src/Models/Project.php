<?php


class Project
{
    private string $naam;
    private string $status = 'actief';
    private string $korteBeschrijving;
    private string $langeBeschrijving;
    private ?string $website;
    private ?string $logo;
    private Company $klant;

    /**
     * Project constructor.
     * @param string $naam
     * @param string $status
     * @param string $korteBeschrijving
     * @param string $langeBeschrijving
     * @param string|null $website
     * @param string|null $logo
     * @param Company $klant
     */
    public function __construct(string $naam, string $status, string $korteBeschrijving, string $langeBeschrijving, ?string $website, ?string $logo, Company $klant)
    {
        $this->naam = $naam;
        $this->status = $status;
        $this->korteBeschrijving = $korteBeschrijving;
        $this->langeBeschrijving = $langeBeschrijving;
        $this->website = $website;
        $this->logo = $logo;
        $this->klant = $klant;
    }

    /**
     * @return string
     */
    public function getNaam(): string
    {
        return $this->naam;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getKorteBeschrijving(): string
    {
        return $this->korteBeschrijving;
    }

    /**
     * @return string
     */
    public function getLangeBeschrijving(): string
    {
        return $this->langeBeschrijving;
    }

    /**
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * @return string|null
     */
    public function getLogo(): ?string
    {
        return $this->logo;
    }

    /**
     * @return Company
     */
    public function getKlant(): Company
    {
        return $this->klant;
    }

    public function __toString(): string
    {
        return $this->naam . ' ' . $this->status . ' ' . $this->korteBeschrijving . ' ' . $this->langeBeschrijving
            . ' ' . $this->website . ' ' . $this->logo . ' ' . $this->klant;
    }
}