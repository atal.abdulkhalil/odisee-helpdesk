<?php
/*
require_once $basePath . 'src/Models/Company.php';
require_once $basePath . 'src/Models/Contact.php';
require_once $basePath . 'src/Models/City.php';*/
require_once __DIR__ . '/../config/database.php';

/**
 * @return array
 */
function getCompanyObjects(): array
{
    $companiesArray = require __DIR__ . '/../resources/data/companies.php';
    $companies = [];
    foreach ($companiesArray as $company) {
        $companies[] = new Company(
            $company['name'],
            $company['address'],
            $company['zip'],
            $company['city'],
            $company['activity'],
            $company['vat']
        );
    }

    return $companies;
}

/**
 * @return array
 */
function getContactObjects(): array
{
    $contactsArray = require __DIR__ . '/../resources/data/contacts.php';
    $contacts = [];
    foreach ($contactsArray as $contact) {
        $contacts[] = new Contact(
            $contact['name'],
            $contact['client'],
            $contact['email'],
            $contact['phone']
        );
    }

    return $contacts;
}

/**
 * @return array
 */
function getCityObjects(): array
{
    $companiesArray = require __DIR__ . '/../resources/data/companies.php';
    $cities = [];
    foreach ($companiesArray as $company) {
        $cities[] = new City(
            $company['city']
        );
    }

    usort($cities, static function ($a, $b) {
        return $a->getName() <=> $b->getName();
    });

    return array_unique($cities);
}

/**
 * @param array $companies
 * @param string $term
 * @return array
 */
function searchCompanies(array $companies, string $term) : array
{
    $visibleCompanies = [];
    foreach ($companies as $company) {
        if (stripos($company->getName(), $term) !== false || stripos($company->getActivity(), $term) !== false) {
            $visibleCompanies[] = $company;
        }
    }
    return $visibleCompanies;
}

/**
 * @param array $companies
 * @param string $city
 * @return array
 */
function searchCompaniesByCity(array $companies, string $city) : array
{
    $visibleCompanies = [];
    foreach ($companies as $company) {
        if (stripos($company->getCity(), $city) !== false) {
            $visibleCompanies[] = $company;
        }
    }
    return $visibleCompanies;
}

/**
 * @param array $companies
 * @param array $provinces
 * @return array
 */
function searchCompaniesByProvinces(array $companies, array $provinces) : array
{
    $visibleCompanies = [];
    foreach ($companies as $company) {
        foreach ($provinces as $province) {
            if (strpos((string)$company->getZip(), $province) === 0) {
                $visibleCompanies[] = $company;
            }
        }
    }
    return $visibleCompanies;
}

/**
 * @param array $contacts
 * @param string $term
 * @return array
 */
function searchContacts(array $contacts, string $term) : array
{
    $visibleContacts = [];
    foreach ($contacts as $contact) {
        if (stripos($contact->getName(), $term) !== false) {
            $visibleContacts[] = $contact;
        }
    }
    return $visibleContacts;
}

function getDBConnection() : \Doctrine\DBAL\Connection {
    $connectionParams = [
        'host' => DB_HOST,
        'dbname' => DB_NAME,
        'user' => DB_USER,
        'password' => DB_PASS,
        'driver' => 'pdo_mysql',
        'charset' => 'utf8mb4'
    ];

    $connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
    try {
        $connection->connect();
        return $connection;
    } catch (\Doctrine\DBAL\Exception\ConnectionException $e) {
        echo 'Could not connect to database';
        exit;
    }
}

function getDBCompanyObjects(): array
{
    $connection = getDBConnection();
    $stmt = $connection->prepare('SELECT * FROM companies ORDER BY `name`');
    $stmt->execute([]);
    $companiesArray = $stmt->fetchAllAssociative();
    $companies = [];
    foreach ($companiesArray as $company) {
        $companies[] = new Company(
            $company['name'],
            $company['address'],
            $company['zip'],
            $company['city'],
            $company['activity'],
            $company['vat'],
            $company['id']
        );
    }

    return $companies;
}

/**
 * @param array $companies
 * @param string $term
 * @return array
 */
function searchDBCompanies(string $term) : array
{
    $connection = getDBConnection();
    $stmt = $connection->prepare('SELECT * FROM companies WHERE `name` LIKE ? ORDER BY `name`');
    $stmt->execute(['%' . $term . '%']);
    $companiesArray = $stmt->fetchAllAssociative();


    return $companiesArray;
}

function searchDBCompaniesByCity(string $city, string $term) : array
{
    $connection = getDBConnection();
    $stmt = $connection->prepare('SELECT * FROM companies WHERE `name` LIKE ? AND `city` LIKE ? ORDER BY `name`');
    $stmt->execute(['%' . $term . '%', '%' . $city . '%']);
    $companiesArray = $stmt->fetchAllAssociative();

    return $companiesArray;
}

/**
 * @param array $companies
 * @param array $provinces
 * @return array
 */