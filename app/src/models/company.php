<?php

class Company {
    private string $name;
    private string $address;
    private int $zip;
    private string $city;
    private string $activity;
    private string $vat;
    private string $id;

    public function __construct(
        string $name,
        string $address,
        int $zip,
        string $city,
        string $activity,
        string $vat,
        string $id
    ) {
        $this->name = $name;
        $this->address = $address;
        $this->zip = $zip;
        $this->city = $city;
        $this->activity = $activity;
        $this->vat = $vat;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getZip(): int
    {
        return $this->zip;
    }

    /**
     * @param int $zip
     */
    public function setZip(int $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getActivity(): string
    {
        return $this->activity;
    }

    /**
     * @param string $activity
     */
    public function setActivity(string $activity): void
    {
        $this->activity = $activity;
    }

    /**
     * @return string
     */
    public function getVat(): string
    {
        return $this->vat;
    }

    /**
     * @param string $vat
     */
    public function setVat(string $vat): void
    {
        $this->vat = $vat;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name . ' ' . $this->address . ' ' . $this->zip . ' ' . $this->city . ' ' .
            $this->activity . ' ' . $this->vat . ' ' . $this->id;
    }

    /**
     * @param $country
     * @return string
     */
    public function getLocaleAddress($country = 'BE'): string
    {
        if ($country === 'FR') {
            $splitAddress = str_split($this->address);
            $number = end($splitAddress);

            // Remove number
            array_pop($splitAddress);
            $address = implode('', $splitAddress);

            return $number . ' ' . $address . ', ' . $this->zip . ' ' . $this->city;
        }

        return $this->address . ' ' . $this->zip . ', ' . $this->city;
    }
}